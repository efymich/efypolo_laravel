<?php

use App\Http\Controllers\Shop\CartController;
use App\Http\Controllers\Shop\LoginController;
use App\Http\Controllers\Shop\ProductController;
use App\Http\Controllers\Shop\ProfileController;
use App\Http\Controllers\Shop\RegistryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('login', LoginController::class)->only([
    'create', 'store', 'destroy'
]);

Route::resource('registration', RegistryController::class)->only([
    'create', 'store',
]);

Route::resource('profile', ProfileController::class)->only([
    'show', 'update', 'edit'
]);

Route::resource('cart', CartController::class)->only([
    'index', 'update', 'destroy'
]);

Route::resource('product', ProductController::class)->only([
    'index', 'show', 'update'
]);
