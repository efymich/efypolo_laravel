<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
//        if (!$_SESSION['user']) {
//            return ['error' => setError('Authorization error')];
//        }
//
//        $email = $_SESSION['user']['email'];
//
//        $query = "SELECT email,first_name,last_name,city,avatar FROM users WHERE email = ? ";
//
//        $result = databaseExecute($query, $email);
//
//        $response = ['profile_card' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
//
//        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
