<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateShopUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_users', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->string('email')->unique()->nullable(false);
            $table->string('last_name')->nullable(false);
            $table->string('first_name')->nullable(false);
            $table->string('password')->nullable(false);
            $table->string('city')->nullable(false);
            $table->string('avatar');
            $table->timestamps();
        });

        DB::statement('ALTER TABLE shop_users ADD CONSTRAINT CHECK (city IN ("Moscow","Saint-Petersburg","Novgorod","Yaroslavl"));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_users');
    }
}
